var assert = require("assert");
var mongoose = require("mongoose");
var should = require("should");
var Collection = require("../../server/models/collection");
var Media = require("../../server/models/media");
var config = require("../../server/config.js");
describe('Structure', function(){

	before(function(done) {
		mongoose.connect(config.dbaddress);							
		done();
	});
	describe('Collection', function(){
		it('should save valid data', function(done){
			var title = 'test'
			var location = {lat: '5', lng: '6'};
			var object = {title: title, location: location};
			new Collection({title: title, location: location}).save(function() {
				Collection.find(object, function(err, collection) {
					var collection = collection[0];

					collection.should.have.property('_id');
					collection.should.have.property('start');
					collection.should.have.property('end');
					collection.should.have.property('location');
					collection.should.have.property('lastupdate');
					collection.should.have.property('path');
				})
				done()
			});
		})
	})
})