/* The API controller
   Exports 3 methods:
   * post - Creates a new thread
   * list - Returns a list of threads
   * show - Displays a thread and its posts
*/
var Collection = require('../models/collection.js');
var Media = require('../models/media.js');
 
exports.post = function(req, res) {
    new Collection({title: req.body.title, author: req.body.author}).save();
}
 
exports.list = function(req, res) {
  Collection.find(function(err, threads) {
    res.send(threads);
  });
}
 
// first locates a thread by title, then locates the replies by thread ID.
exports.show = function(req, res) {
    Collection.findOne({title: req.params.title}, function(error, thread) {
        var posts = Media.find({thread: thread._id}, function(error, posts) {
          res.send([{thread: thread, posts: posts}]);
        });
    })
};