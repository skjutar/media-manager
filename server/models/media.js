// The Post model
 
var mongoose = require('mongoose')
   ,Schema = mongoose.Schema
   ,ObjectId = Schema.ObjectId;
 
var mediaSchema = new Schema({
    album: ObjectId,
    runningnumber: {type: Number, default: 1},
    date: {type: Date, default: Date.now},
    location: {lat: String, lng: String}
});

mediaSchema.virtual('path').get(function () {
  return '#'+this.runningnumber;
});

module.exports = mongoose.model('Media', mediaSchema);