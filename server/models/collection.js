// The Thread model
 
var mongoose = require('mongoose')
  , Schema = mongoose.Schema;
 
var collectionSchema = new Schema({
    title:  String,
    start: {type: Date, default: Date.now},
    end: {type: Date, default: Date.now},
    location: { lat: String, lng: String },
    lastupdate: {type: Date, default: Date.now}
});

collectionSchema.virtual('path').get(function () {
  return this.title+'#'+this.start.getMonth()+'/'+this.start.getFullYear();
});
 
module.exports = mongoose.model('Collection', collectionSchema);